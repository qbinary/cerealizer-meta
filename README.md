# Ceralizer aka. Cereal-Tron-3000 
## Ceralizer what?
The name 'Cerealizer' is a "haha funny" programming pun originated in the word "Serializer". Because haha "Cereal ~ Serial" got it?  

## Idea
The 'Cerealizer' is a machine for automatic cereal mixing. The idea is that you have a few different ingredients stored in some kind of silos which can then be individually dispensed into a kind of funnel which ends up in a bowl.  

## Realisation
#### Silos
For the silos I decided to use pipes out of transparent acrylic which can be variated in height to increase the amount of cereal that can be held.

#### Dispenser
The dispenser is a custom 3D modeled mechanism which consists of a funnel ending in a spiral helix. The spiral helix is then rotated by a stepper motor attached to that mechanism. At the end of the spiral helix is a hole where the ingredient can then drop down into the bowl.

#### Control Hardware
As my main MCU I use a socketed 'Wemos D1 mini pro' ESP8266X based microcontroller with integrated WiFi. This MCU is placed onto a custom printed circuit board which holds 8 stepper control boards for a maximum of 8 ingredients.  
Because of the strictly limited amount of pins on the MCU I had to use a 8bit shift register to be able to properly control all eight stepper motors individually.

## Associated Projects
### CerealOS
It's not an operating system. It' just a firmware made in PlatformIO with Arduion C++. But I wanted to name it CerealOS so deal with it!
> :package: [ CerealOS Repository ](https://codeberg.org/qbinary/cereal-os)

## Color pallette
### Raw
| Description         | Name      | html hex code | Nextion color code |
| ------------------- | --------- | ------------- | ------------------ |
| Primary Accent      | Brass     | bb8e57        | 48234              |
| Secondary Accent    | Corrosion | 61806a        | 25613              |
| Additional Accent 1 | Gold      | d89840        | -                  |
| Additional Accent 2 | Amber     | b36d41        | 45928              |
| Additional Accent 3 | Coffee    | 7d4735        | 31270              |
| General Text        | Blight    | bcb6b3        | 48566              |
| General Background  | Titanium  | 3d342e        | 14757              |
| Alert Text          | Bloodmoon | bb5757        | -                  |

### Color pallette
![Color pallette image](https://codeberg.org/qbinary/cerealizer-meta/raw/branch/qbinary-readme-1/color_pallette.png)
